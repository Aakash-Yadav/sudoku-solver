# Sudoku Solver

### What is a Sudoku Puzzle ?
> In the Sudoku puzzle we need to fill in every empty
> box with an integer between 1 and 9 in such a way that every
> number from 1 up to 9 appears once in every row, every column
> and every one of the small 3 by 3 boxes highlighted with thick
> borders.

> The difficulty of this puzzle might vary. The more the difficulty level of Sudoku puzzles, the more challenging research problem it becomes
> for the computational scientists. Difficult puzzles mostly have less
> prescribed symbols.
> 
> The Sudoku puzzles which are published for entertainment
> have unique solutions. A Sudoku puzzle is believed to be a well-formed if it has a unique solution. Another challenging research problem is to determine how few boxes need to be filled for a Sudoku puzzle to be well-formed. Well-formed Sudoku with 17 symbols exist. It is unknown whether or not there exists a well formed puzzle with only 16 clues.


### Steps to solve the sudoku puzzle in Python
- In this method for solving the sudoku puzzle,
-  first we assign the size of the 2D matrix to variable M (M*M).
- Then we assign the utility function (puzzle) to print the grid.
- Later it will assign num to the row and col.
- If we find same num in the same row or same column or in the specific 3*3 matrix, ‘false’ will be returned.
- Then we will check if we have reached the 8th row and 9th column and return true for stopping the further backtracking.
- Next we will check if the column value becomes 9 then we move to the next row and column.
- Further now we see if the current position of the grid has value greater than 0, then we iterate for next column.
- After checking if it is a safe place , we move to the next column and then assign num in current (row ,col) position of the grid. Later we check for next possibility with next column.
- As our assumption was wrong, we discard the assigned num and then we go for the next assumption with different num value

### Implementing the sudoku solver in Python
> _We’ll use the backtracking method to create our sudoku solver in Python. Backtracking means switching back to the previous step as soon as we determine that our current solution cannot be continued into a complete one. We use this principle of backtracking to implement the sudoku algorithm._

### Code:/
  ![alt text](https://xp.io/storage/1ADAkt5d.png)

**Conclusion**
> And that’s it for building a sudoku solver in Python! I hope you had fun reading through the article and learned  how we implemented the code.


import numpy as np
import time
class SUDKO(object):
    sudko =     [[2, 5, 0, 0, 3, 0, 9, 0, 1],
                [0, 1, 0, 0, 0, 4, 0, 0, 0],
                [4, 0, 7, 0, 0, 0, 2, 0, 8],
                [0, 0, 5, 2, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 9, 8, 1, 0, 0],
                [0, 4, 0, 0, 0, 3, 0, 0, 0],
                [0, 0, 0, 3, 6, 0, 0, 7, 2],
                [0, 7, 0, 0, 0, 0, 0, 0, 3],
                [9, 0, 3, 0, 0, 0, 6, 0, 4]]
    pass
class Return_true_false(SUDKO):
    def __init__(self,row,column,number):
        self.row,self.column ,self.number=row,column,number
    def poss(self):
        for i in range(9):
            if self.sudko[self.row][i] == self.number:
                return False
        for i in range(9):
            if self.sudko[i][self.column] == self.number:
                return False
        Box_x,Box_y = (self.column//3)*3,(self.row//3)*3
        for i in range(3):
            for j in range(3):
                if self.sudko[Box_y+i][Box_x+j]==self.number:
                    return False
        return True
def Solving():
    for i in range(9):
        for j in range(9):
            if Return_true_false.sudko[i][j]==0:
                for n in range(1,10):
                    if Return_true_false(i,j,n).poss()==True:
                        Return_true_false.sudko[i][j]=n
                        Solving()
                        Return_true_false.sudko[i][j]=0
                return 
    print(np.matrix(Return_true_false.sudko))
print('The Given sudko')
print(np.matrix(Return_true_false.sudko))
print('')
time.sleep(2)
print('Solved sudko')
Solving()
        
        
    
    
    
    